using BlueSoft.Data;
using BlueSoft.Interfaces;
using BlueSoft.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace BlueSoft
{
    public class Startup
    {
        public Startup(IConfiguration configuration) => Configuration = configuration;

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BlueSoftDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("BlueSoftDataBase")));
            services.AddScoped<IBaseRepository<Author>, AuthorRepository>();
            services.AddScoped<IBaseRepository<Book>, BookRepository>();
            services.AddScoped<IBaseRepository<Category>, CategoryRepository>();
            services.AddControllers();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Info BlueSoft Service API",
                    Version = "v1",
                    Description = "Info BlueSoft Service API",
                });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v1/swagger.json", "Info BlueSoft Service API"));

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            loggerFactory.AddLog4Net();
        }
    }
}
