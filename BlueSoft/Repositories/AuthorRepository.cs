﻿using BlueSoft.Data;
using BlueSoft.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlueSoft.Repositories
{
    public class AuthorRepository : IBaseRepository<Author>
    {
        private readonly BlueSoftDbContext _blueSoftDbContext;

        public AuthorRepository(BlueSoftDbContext blueSoftDbContext) => _blueSoftDbContext = blueSoftDbContext;

        public async Task<ICollection<Author>> GetAll() => await _blueSoftDbContext.Authors.ToListAsync();

        public async Task<Author> Get(int id) => await _blueSoftDbContext.Authors.FirstOrDefaultAsync(c => c.Id == id);

        public async Task Add(Author entity)
        {
            _blueSoftDbContext.Authors.Add(entity);
            await _blueSoftDbContext.SaveChangesAsync();
        }

        public async Task Update(Author oldEntity, Author newEntity)
        {
            oldEntity.Name = newEntity.Name;
            oldEntity.LastName = newEntity.LastName;
            oldEntity.Birthday = newEntity.Birthday;

            await _blueSoftDbContext.SaveChangesAsync();
        }

        public async Task Delete(Author entity)
        {
            _blueSoftDbContext.Authors.Remove(entity);
            await _blueSoftDbContext.SaveChangesAsync();
        }
    }
}
