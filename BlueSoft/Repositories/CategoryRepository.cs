﻿using BlueSoft.Data;
using BlueSoft.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlueSoft.Repositories
{
    public class CategoryRepository : IBaseRepository<Category>
    {
        private readonly BlueSoftDbContext _blueSoftDbContext;

        public CategoryRepository(BlueSoftDbContext blueSoftDbContext) => _blueSoftDbContext = blueSoftDbContext;

        public async Task<ICollection<Category>> GetAll() => await _blueSoftDbContext.Categories.ToListAsync();

        public async Task<Category> Get(int id) => await _blueSoftDbContext.Categories.FirstOrDefaultAsync(c => c.Id == id);

        public async Task Add(Category entity)
        {
            _blueSoftDbContext.Categories.Add(entity);
            await _blueSoftDbContext.SaveChangesAsync();
        }

        public async Task Update(Category oldEntity, Category newEntity)
        {
            oldEntity.Name = newEntity.Name;
            oldEntity.Description = newEntity.Description;
            await _blueSoftDbContext.SaveChangesAsync();
        }

        public async Task Delete(Category entity)
        {
            _blueSoftDbContext.Categories.Remove(entity);
            await _blueSoftDbContext.SaveChangesAsync();
        }
    }
}
