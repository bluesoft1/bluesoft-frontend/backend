﻿using BlueSoft.Data;
using BlueSoft.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlueSoft.Repositories
{
    public class BookRepository : IBaseRepository<Book>
    {
        private readonly BlueSoftDbContext _blueSoftDbContext;

        public BookRepository(BlueSoftDbContext blueSoftDbContext) => _blueSoftDbContext = blueSoftDbContext;

        public async Task<ICollection<Book>> GetAll() => await _blueSoftDbContext.Books.ToListAsync();

        public async Task<Book> Get(int id) => await _blueSoftDbContext.Books.FirstOrDefaultAsync(c => c.Id == id);

        public async Task Add(Book entity)
        {
            _blueSoftDbContext.Books.Add(entity);
            await _blueSoftDbContext.SaveChangesAsync();
        }

        public async Task Update(Book oldEntity, Book newEntity)
        {
            oldEntity.Name = newEntity.Name;
            oldEntity.Author = newEntity.Author;
            oldEntity.Categories = newEntity.Categories;
            oldEntity.ISBN = newEntity.ISBN;

            await _blueSoftDbContext.SaveChangesAsync();
        }

        public async Task Delete(Book entity)
        {
            _blueSoftDbContext.Books.Remove(entity);
            await _blueSoftDbContext.SaveChangesAsync();
        }
    }
}
