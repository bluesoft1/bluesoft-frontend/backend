﻿using Microsoft.EntityFrameworkCore;

namespace BlueSoft.Data
{
    public class BlueSoftDbContext : DbContext
    {
        public BlueSoftDbContext(DbContextOptions dbContextOptions) : base(dbContextOptions) { }

        public DbSet<Book> Books { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<Category> Categories { get; set; }
    }
}