﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlueSoft.Data
{
    public class Book
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

        public Author Author { get; set; }

        public ICollection<Category> Categories { get; set; }

        public string ISBN { get; set; }

        public Book() => Categories = new HashSet<Category>();
    }
}
