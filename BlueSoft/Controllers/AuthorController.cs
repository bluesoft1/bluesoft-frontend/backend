﻿using BlueSoft.Data;
using BlueSoft.Enumerations;
using BlueSoft.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlueSoft.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IBaseRepository<Author> _repository;
        private readonly ILogger _logger;

        public AuthorController(IBaseRepository<Author> repository, ILogger<AuthorController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("GetAuthors")]
        public async Task<ICollection<Author>> GetAuthors() => await _repository.GetAll();

        [HttpGet("{id}")]
        public async Task<ActionResult<Author>> GetAuthor(int id)
        {
            Author author = await _repository.Get(id);

            if (author == null)
                return NotFound();

            return author;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAuthor(int id, Author newAuthor)
        {
            if (id != newAuthor.Id)
                return BadRequest();

            Author oldAuthor = await _repository.Get(id);

            if (oldAuthor == null)
                return NotFound();

            try
            {
                await _repository.Update(oldAuthor, newAuthor);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error : { ex.Message}");
                return Problem(detail: MessagesEnum.CustomError);
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Author>> PostAuthor(Author author)
        {
            try
            {
                await _repository.Add(author);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error : { ex.Message}");
                return Problem(detail: MessagesEnum.CustomError);
            }

            return CreatedAtAction("GetAuthor", new { id = author.Id }, author);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Author>> DeleteAuthor(int id)
        {
            Author author = await _repository.Get(id);

            if (author == null)
                return NotFound();

            try
            {
                await _repository.Delete(author);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error : { ex.Message}");
                return Problem(detail: MessagesEnum.CustomError);
            }

            return Ok();
        }
    }
}
