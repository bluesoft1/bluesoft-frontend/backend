﻿using BlueSoft.Data;
using BlueSoft.Enumerations;
using BlueSoft.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlueSoft.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly IBaseRepository<Category> _repository;
        private readonly ILogger _logger;

        public CategoryController(IBaseRepository<Category> repository, ILogger<AuthorController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ICollection<Category>> GetCategorys() => await _repository.GetAll();

        [HttpGet("{id}")]
        public async Task<ActionResult<Category>> GetCategory(int id)
        {
            Category Category = await _repository.Get(id);

            if (Category == null)
                return NotFound();

            return Category;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategory(int id, Category newCategory)
        {
            if (id != newCategory.Id)
                return BadRequest();

            Category oldCategory = await _repository.Get(id);

            if (oldCategory == null)
                return NotFound();

            try
            {
                await _repository.Update(oldCategory, newCategory);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error : { ex.Message}");
                return Problem(detail: MessagesEnum.CustomError);
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Category>> PostCategory(Category category)
        {
            try
            {
                await _repository.Add(category);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error : { ex.Message}");
                return Problem(detail: MessagesEnum.CustomError);
            }

            return CreatedAtAction("GetCategory", new { id = category.Id }, category);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Category>> DeleteCategory(int id)
        {
            Category category = await _repository.Get(id);

            if (category == null)
                return NotFound();

            try
            {
                await _repository.Delete(category);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error : { ex.Message}");
                return Problem(detail: MessagesEnum.CustomError);
            }

            return Ok();
        }
    }
}
