﻿using BlueSoft.Data;
using BlueSoft.Enumerations;
using BlueSoft.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlueSoft.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBaseRepository<Book> _repository;
        private readonly ILogger _logger;

        public BookController(IBaseRepository<Book> repository, ILogger<AuthorController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("GetBooks")]
        //public async Task<ICollection<Book>> GetBooks() => await _repository.GetAll();
        public async Task<ICollection<Book>> GetBooks()
        {
            ICollection<Book> res = await _repository.GetAll();
            return res;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Book>> GetBook(int id)
        {
            Book book = await _repository.Get(id);

            if (book == null)
                return NotFound();

            return book;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutBook(int id, Book newBook)
        {
            if (id != newBook.Id)
                return BadRequest();

            Book oldBook = await _repository.Get(id);

            if (oldBook == null)
                return NotFound();

            try
            {
                await _repository.Update(oldBook, newBook);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error : { ex.Message}");
                return Problem(detail: MessagesEnum.CustomError);
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Book>> PostBook(Book book)
        {
            try
            {
                await _repository.Add(book);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error : { ex.Message}");
                return Problem(detail: MessagesEnum.CustomError);
            }

            return CreatedAtAction("GetBook", new { id = book.Id }, book);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Book>> DeleteBook(int id)
        {
            Book book = await _repository.Get(id);

            if (book == null)
                return NotFound();

            try
            {
                await _repository.Delete(book);
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Error : { ex.Message}");
                return Problem(detail: MessagesEnum.CustomError);
            }

            return Ok();
        }
    }
}
