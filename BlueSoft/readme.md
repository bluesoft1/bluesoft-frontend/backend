# BlueSoft Library API REST

BlueSoft es una solución con un proyecto API REST construido en .Net Core Version 3.1 con la finalidad de exponer un CRUD sobre los datos de una biblioteca.

## Instrucciones de compilación

Se debe contar con el SDK de .Net Core 3.1 y acceso a Internet para descargar los paquetes necesarios de compilación de la solución.

```console
NuGet.exe install "BlueSoft/packages.config" -o packages/
dotnet build
```

## Ejecución de migraciones

```console
add-migration InitialStructure -Context BlueSoftDbContext
update-database -Context BlueSoftDbContext
```

## Ejecución del proyecto

Puede ejecutarse con el IDE Visual Studio 2019 o con la linea de comando

```console
dotnet run
```