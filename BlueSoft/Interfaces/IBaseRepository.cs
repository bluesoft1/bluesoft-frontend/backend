﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlueSoft.Interfaces
{
    public interface IBaseRepository<T>
    {
        Task<ICollection<T>> GetAll();
        Task<T> Get(int id);
        Task Add(T entity);
        Task Update(T oldEntity, T newEntity);
        Task Delete(T entity);
    }
}
